﻿
namespace Bowling
{
	public class Game
	{
		protected Frame firstFrame;

		public Game(Frame firstFrame)
		{
			this.firstFrame = firstFrame;
		}

		public int GetTotalScore()
		{
			var totalScore = 0;
			var frame = firstFrame;
			totalScore += frame.CalculateScore();

			while (frame.HasNext)
			{
				frame = frame.NextFrame;
				totalScore += frame.CalculateScore();
			}

			return totalScore;
		}
	}
}
