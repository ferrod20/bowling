﻿using System;

namespace Bowling
{
	class Program
	{
		static void Main(string[] args)
		{
			var parser = new GameParser();
			if (args.Length == 0)
				Console.WriteLine("No input provided.");
			else
			{
				var game = parser.Parse(args[0]);
				var totalScore = game.GetTotalScore();
				Console.WriteLine($"Total score: {totalScore}");
			}
		}
	}
}