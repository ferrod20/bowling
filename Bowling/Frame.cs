﻿
namespace Bowling
{
	public abstract class Frame
	{
		protected Frame nextFrame;
		public Frame NextFrame => nextFrame;

		public bool HasNext => nextFrame != null;
		public abstract int FirstBallScore();
		public abstract int SecondBallScore();
		public abstract int CalculateScore();

		public void AddNextFrame(Frame frame)
		{
			nextFrame = frame;
		}
	}

	public class RegularFrame : Frame
	{
		private int firstBallScore;
		private int secondBallScore;

		public RegularFrame(int firstBallScore, int secondBallScore)
		{
			this.firstBallScore = firstBallScore;
			this.secondBallScore = secondBallScore;
		}

		public override int FirstBallScore()
		{
			return firstBallScore;
		}

		public override int SecondBallScore()
		{
			return secondBallScore;
		}

		public override int CalculateScore()
		{
			return firstBallScore + secondBallScore;
		}
	}

	public class Strike : Frame
	{
		public override int FirstBallScore()
		{
			return 10;
		}

		public override int SecondBallScore()
		{
			return nextFrame.FirstBallScore();
		}

		public override int CalculateScore()
		{
			return 10 + nextFrame.FirstBallScore() + nextFrame.SecondBallScore();
		}
	}

	public class Spare : Frame
	{
		private int firstBallScore;

		public Spare(int firstBallScore)
		{
			this.firstBallScore = firstBallScore;
		}

		public override int FirstBallScore()
		{
			return firstBallScore;
		}

		public override int SecondBallScore()
		{
			return 10 - firstBallScore;
		}

		public override int CalculateScore()
		{
			return 10 + nextFrame.FirstBallScore();
		}
	}

	public class BonusFrame : RegularFrame
	{
		
		public BonusFrame(int firstBallScore, int secondBallScore) : base(firstBallScore, secondBallScore)
		{
		}

		public BonusFrame(int firstBallScore) : base(firstBallScore, 0)
		{
		}

		public override int CalculateScore()
		{
			return 0;
		}

	}
}
