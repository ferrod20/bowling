﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bowling.Test
{
	[TestClass]
	public class FrameTest
	{
		[TestMethod]
		public void RegularFrameFirstAndSecondScoresAreCorrect()
		{
			Frame frame = new RegularFrame(2, 3);

			Assert.AreEqual(2, frame.FirstBallScore()); 
			Assert.AreEqual(3, frame.SecondBallScore()); 
		}

		[TestMethod]
		public void StrikeFirstAndSecondScoresAreCorrect()
		{
			Frame frame = new Strike();
			frame.AddNextFrame(new RegularFrame(2,3));

			Assert.AreEqual(10, frame.FirstBallScore());
			Assert.AreEqual(2, frame.SecondBallScore());
		}

		[TestMethod]
		public void StrikeWithBonusFirstAndSecondScoresAreCorrect()
		{
			Frame frame = new Strike();
			frame.AddNextFrame(new BonusFrame(2, 3));

			Assert.AreEqual(10, frame.FirstBallScore());
			Assert.AreEqual(2, frame.SecondBallScore());
		}

		[TestMethod]
		public void SpareFirstAndSecondScoresAreCorrect()
		{
			Frame frame = new Spare(4);

			Assert.AreEqual(4, frame.FirstBallScore());
			Assert.AreEqual(6, frame.SecondBallScore());
		}

		[TestMethod]
		public void StrikeFollowedByRegularScoreIsCorrect()
		{
			Frame frame = new Strike();
			frame.AddNextFrame(new RegularFrame(2, 3));

			Assert.AreEqual(15, frame.CalculateScore());
		}

		[TestMethod]
		public void StrikeFollowedBySpareScoreIsCorrect()
		{
			Frame frame = new Strike();
			frame.AddNextFrame(new Spare(2));

			Assert.AreEqual(20, frame.CalculateScore());
		}

		[TestMethod]
		public void StrikeFollowedByStrikeThenRegularScoreIsCorrect()
		{
			Frame frame = new Strike();
			var nextFrame = new Strike();
			frame.AddNextFrame(nextFrame);
			nextFrame.AddNextFrame(new RegularFrame(1,0));

			Assert.AreEqual(21, frame.CalculateScore());
		}

		[TestMethod]
		public void StrikeFollowedByStrikeThenSpareScoreIsCorrect()
		{
			Frame frame = new Strike();
			var nextFrame = new Strike();
			frame.AddNextFrame(nextFrame);
			nextFrame.AddNextFrame(new Spare(3));

			Assert.AreEqual(23, frame.CalculateScore());
		}

		[TestMethod]
		public void StrikeFollowedBy2StrikeScoreIsCorrect()
		{
			Frame frame = new Strike();
			var nextFrame = new Strike();
			frame.AddNextFrame(nextFrame);
			nextFrame.AddNextFrame(new Strike());

			Assert.AreEqual(30, frame.CalculateScore());
		}


		[TestMethod]
		public void SpareFollowedByRegularScoreIsCorrect()
		{
			Frame frame = new Spare(4);
			frame.AddNextFrame(new RegularFrame(2, 3));

			Assert.AreEqual(12, frame.CalculateScore());
		}

		[TestMethod]
		public void SpareFollowedByBonusScoreIsCorrect()
		{
			Frame frame = new Spare(4);
			frame.AddNextFrame(new BonusFrame(2, 3));

			Assert.AreEqual(12, frame.CalculateScore());
		}

		[TestMethod]
		public void SpareFollowedBySpareScoreIsCorrect()
		{
			Frame frame = new Spare(4);
			frame.AddNextFrame(new Spare(2));

			Assert.AreEqual(12, frame.CalculateScore());
		}

		[TestMethod]
		public void SpareFollowedByStrikeScoreIsCorrect()
		{
			Frame frame = new Spare(4);
			frame.AddNextFrame(new Strike());

			Assert.AreEqual(20, frame.CalculateScore());
		}
	}
}
