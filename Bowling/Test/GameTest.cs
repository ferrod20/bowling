﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bowling.Test
{
	[TestClass]
	public class GameTest
	{
		[TestMethod]
		public void TotalScoreEqualsScoreOfAllFrames()
		{
			var frame1 = new RegularFrame(2, 3);
			var frame2 = new Strike();
			var frame3 = new Spare(4);
			var frame4 = new BonusFrame(2);

			frame3.AddNextFrame(frame4);
			frame2.AddNextFrame(frame3);
			frame1.AddNextFrame(frame2);

			var totalScore = frame1.CalculateScore() + frame2.CalculateScore() + frame3.CalculateScore();

			var game = new Game(frame1);
			Assert.AreEqual(totalScore, game.GetTotalScore()); 
		}
	}
}
