﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bowling.Test
{
	[TestClass]
	public class IntegrationTest
	{
		[TestMethod]
		public void AllStrikesGameScoreCalculationIsCorrect()
		{
			var parser = new GameParser();
			var game = parser.Parse("X|X|X|X|X|X|X|X|X|X||XX");
			Assert.AreEqual(300, game.GetTotalScore()); 
		}

		[TestMethod]
		public void AllRegularsGameScoreCalculationIsCorrect()
		{
			var parser = new GameParser();
			var game = parser.Parse("9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||");
			Assert.AreEqual(90, game.GetTotalScore());
		}

		[TestMethod]
		public void AllSparesGameScoreCalculationIsCorrect()
		{
			var parser = new GameParser();
			var game = parser.Parse("5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5");
			Assert.AreEqual(150, game.GetTotalScore());
		}

		[TestMethod]
		public void MixedFramesGameScoreCalculationIsCorrect()
		{
			var parser = new GameParser();
			var game = parser.Parse("X|7/|9-|X|-8|8/|-6|X|X|X||81");
			Assert.AreEqual(167, game.GetTotalScore());
		}
	}
}
