﻿using System;
using System.Collections.Generic;

namespace Bowling
{
	//TODO: Check for valid strings (put special focus on games with bonuses)
	//TODO: Add exceptions messages
	//TODO: Add unit tests
	public class GameParser
	{
		public Game Parse(string input)
		{
			Game game = null;
			var framesAndBonus = input.Split(new[] { "||" }, StringSplitOptions.None);

			if (framesAndBonus.Length != 2)
				throw new Exception();

			var strFrames = new List<string>(framesAndBonus[0].Split('|'));
			var strBonus = framesAndBonus[1];

			if (!string.IsNullOrWhiteSpace(strBonus))
				strFrames.Add("|" + strBonus);

			Frame currentFrame = null;
			foreach (var strFrame in strFrames)
			{
				var frame = GetFrame(strFrame);

				if (currentFrame != null)
					currentFrame.AddNextFrame(frame);
				else
					game = new Game(frame);

				currentFrame = frame;
			}

			return game;
		}

		private Frame GetFrame(string strFrame)
		{
			Frame frame = null;
			if (strFrame.StartsWith("|"))// Bonus frame
			{
				switch (strFrame.Length)
				{
					case 2:
						frame = new BonusFrame(ParseScore(strFrame[1]));
						break;

					case 3:
						frame = new BonusFrame(ParseScore(strFrame[1]), ParseScore(strFrame[2]));
						break;
					default:
						throw new Exception();
				}
			}
			else
			{
				switch (strFrame.Length)
				{
					case 1: //Strike
						if (strFrame == "X")
							frame = new Strike();
						else
							throw new Exception();
						break;
					case 2:
						var firstBall = strFrame[0];
						var secondBall = strFrame[1];
						if (IsAlowedDigit(firstBall))
						{
							if (IsAlowedDigit(secondBall))
								frame = new RegularFrame(ParseScore(firstBall), ParseScore(secondBall));
							else if (secondBall == '/')
								frame = new Spare(ParseScore(firstBall));
							else
								throw new Exception();

						}
						break;
					default:
						throw new Exception();
				}
			}

			return frame;
		}

		private int ParseScore(char ball)
		{
			return ball == '-' ? 0 : ball == 'X' ? 10 : int.Parse(ball.ToString());
		}

		private bool IsAlowedDigit(char ball)
		{
			return (char.IsDigit(ball) && ball != 0) || ball == '-';
		}
	}
}
